from django import forms
from tasks.models import Task


class CreateForm(forms.ModelForm):
    class Meta(Task):
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
