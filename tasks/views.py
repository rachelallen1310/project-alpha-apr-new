from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.models import Project
from tasks.forms import CreateForm

# Create your views here.


@login_required
def TaskView(request, id):
    project_view = Project.objects.get(id=id)
    task_view = Task.objects.all()
    context = {"tasks": task_view, "project": project_view}
    return render(request, "tasks/detail.html", context)


@login_required
def CreateTask(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def MyTask(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task,
    }
    return render(request, "tasks/list.html", context)
