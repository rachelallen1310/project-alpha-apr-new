from django.urls import path
from tasks.views import CreateTask, MyTask


urlpatterns = (
    path("create/", CreateTask, name="create_task"),
    path("mine/", MyTask, name="show_my_tasks"),
)
