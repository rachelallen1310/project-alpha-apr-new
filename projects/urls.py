from django.urls import path
from projects.views import ProjectView, CreateProject
from tasks.views import TaskView

urlpatterns = (
    path("", ProjectView, name="list_projects"),
    path("<int:id>/", TaskView, name="show_project"),
    path("create/", CreateProject, name="create_project"),
)
