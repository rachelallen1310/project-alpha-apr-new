from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateForm

# Create your views here.


@login_required
def ProjectView(request):
    project_view = Project.objects.filter(owner=request.user)
    context = {
        "projects": project_view,
    }
    return render(request, "projects/list.html", context)


@login_required
def CreateProject(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
